const firebase = require('firebase/app')
require('firebase/database')
const config = require('../../config')

firebase.initializeApp(config.firebase)
const database = firebase.database()

module.exports.save = (links) => {
  Object.keys(links)
  .forEach(function eachKey(key) {
    firebase.database().ref('linkList/'+key).set(links[key])
  })
}

module.exports.load = (data, url = '') => {
  firebase.database().ref('/linkList/' + url).once('value').then((snapshot) => {
    return (snapshot.val() && snapshot.val()) || false;
  }).then((value) => {
    console.log('valor  ' + value)
    data(value)
  })
}
