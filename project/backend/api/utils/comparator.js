module.exports.compare = (a, b) => {
  if ( a.visits < b.visits ){
    return 1;
  }
  if ( a.visits > b.visits ){
    return -1;
  }
  return 0;
}
