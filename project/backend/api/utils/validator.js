module.exports.urlValidation = (link, valid) => {
  console.log('receiving link : ' + link)
  var url = require("url");
  var result = url.parse(link);
  if(result.hostname){
    var request = require('request');
    request({method: 'HEAD', uri:link}, (error, response, body) => {
      if (!error) {
        console.log('status online')
        valid(true)
      } else {
        console.log('status offline')
        valid(false)
      }
    })
  } else {
    valid(false)
  }
}
