const express = require('express')
const router = express.Router()
const crypto = require('crypto')
const shortid = require('shortid')
const bodyParser = require('body-parser')
const database = require('../database')
const utils = require('../utils')

let linkListArray = []
linkListArray = database.load((data) =>{
  linkListArray = data
})

router.use(bodyParser.json())
router.use(bodyParser.urlencoded({extended: false}))
router.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*")
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE')
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
        next()
})

router.get('/links', (req, res) => {
  database.load((data) => {
    let listResponse = []
    console.log(data)
    for (var link in data) {
      listResponse.push({ link: [link][0], visits: data[link].visits })
    }
    res.status(200).json(listResponse)
  })
})

router.get('/links/top', (req, res) => {
  database.load((data) => {
    let dataArray = []
    for (var link in data) {
      dataArray.push({ link: [link][0], visits: data[link].visits })
    }
    let top5 = dataArray.sort(utils.comparator.compare).slice(0, 5)
    listResponse = top5.map((item) => { return {link: item.link, visits: item.visits} })
    res.status(200).json({ linkList: listResponse})
  })
})

router.get('/:id', (req, res) => {
  let shrinkId = req.params.id
  database.load((data) => {
    if(data){
      console.log('Redirecting to ' + data.link + ' (' + shrinkId + ')...')
      data.visits++
      database.save({[shrinkId] : data})
      res.send('<meta http-equiv="refresh" content="2; url=' + data.link + '" />Redirecting to ' + data.link + ' (' + shrinkId + ')...')
    } else {
        res.status(404).send('This link not exist')
    }
    res.end()
  }, shrinkId)
})

router.post('/links', (req, res) => {
  utils.validator.urlValidation(req.body.link, (valid => {

    console.log(valid)

    if(valid){
      database.load((data) => {
        var shrinkHash = shortid.generate()
        var linkJson = { [shrinkHash]: { link : req.body.link, visits: '0' }}
        database.save(linkJson)
        res.status(200).json({ link : shrinkHash })
      })
    } else {
      res.status(400).end()
    }
  })
)})

module.exports = router
