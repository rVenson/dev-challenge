const express = require('express')
const router = require('./api/routes/routes.js')
const config = require('./api/config')

const server = express()

server.use(router)
server.listen(config.server_port, () =>{
  console.log('Backend listening in port ' + config.server_port)
})
