import React from 'react'

export default class LinkList extends React.Component {
  constructor(props){
    super(props)
  }

  render(){

    this.state = {
      linkList: this.props.linkList
    }

    const serverUrl = this.props.serverUrl

    return(
      <div>
        {
          this.state.linkList.map((link) => {
            let linkUrl = serverUrl + link.link
            return <div class='row d-flex justify-content-between border-bottom link-item'>
              <div class='p2'><a href={linkUrl} target='_blank'>{linkUrl}</a></div>
              <div class='p2'>{link.visits}</div>
            </div>
          })
        }
      </div>
    );
  }
}
